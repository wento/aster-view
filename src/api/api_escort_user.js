import * as API from './'

export default {
  /**
   * 获取陪护员列表（分页）
   */
  findList:params=>{
    return API.GET('/api-service-manage/escortUser',params)
  },

  addUser:params=>{
    return API.POST('/api-service-manage/escortUser/add',params)
  },

  editUser:params=>{
    return API.POST('/api-service-manage/escortUser/edit',params)
  },

  deleteUser:params=>{
    return API.POST('/api-service-manage/escortUser/delete',params)
  },

  getById:params=>{
    return API.GET('/api-service-manage/escortUser/getById',params)
  },

  changeState:params=>{
    return API.POST('/api-service-manage/escortUser/changeState',params)
  }
}
