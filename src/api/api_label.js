import * as API from './'

export default {

  /**
   * 获取医院列表（分页）
   */
  findList:params=>{
    return API.GET('/api-service-manage/labelInfo',params)
  },

  getList:params=>{
    return API.GET('/api-service-manage/labelInfo/getList',params)
  },

  addLabel:params=>{
    return API.POST('/api-service-manage/labelInfo/add',params)
  },

  editLabel:params=>{
    return API.POST('/api-service-manage/labelInfo/edit',params)
  },

  deleteLabel:params=>{
    return API.POST('/api-service-manage/labelInfo/delete',params)
  },

  getById:params=>{
    return API.GET('/api-service-manage/labelInfo/getById',params)
  },

  changeState:params=>{
    return API.POST('/api-service-manage/labelInfo/changeState',params)
  }
}
