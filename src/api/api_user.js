/**
 * Created by bootdo.
 * 用户相关api
 */
import * as API from './'

export default {
  //登录
  login: params => {
    return API.POST('/api-base/login', params)
  },
  //登出
  logout: params => {
    return API.GET('/api-base/logout', params)
  },
  tokenUser: params =>{
    return API.GET('/api-base/user/currentUser',params)
  },
  //修改个人信息
  changeProfile: params => {
    return API.PUT('/api-base/user/profile', params)
  },

  //查询获取user列表(通过page分页)
  findList: params => {
    return API.GET('/api-base/user', params)
  },

  //增加用户
  addUser:params =>{
    return API.POST('/api-base/user',params)
  },
  //修改用户
  editUser:params =>{
    return API.PUT('/api-base/user',params)
  },
  //修改用户
  resetPsw:params =>{
    return API.PUT('/api-base/user/reset',params)
  },
  //删除用户
  removeUser:params =>{
    return API.DELETE('/api-base/user',params)
  },
  // 修改密码
  changePassword:params =>{
    return API.PUT('/api-base/user/changePwd',params)
  }
}
