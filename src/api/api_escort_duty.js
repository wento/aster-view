import * as API from './'

export default {

  getList:params=>{
    return API.GET('/api-service-manage/userDuty/getDutyList',params)
  },

  editState:params=>{
    return API.POST('/api-service-manage/userDuty/editDutyState',params)
  },

}
