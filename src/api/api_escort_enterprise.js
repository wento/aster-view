import * as API from './'

export default {
  /**
   * 获取陪护企业列表（分页）
   */
  findList:params=>{
    return API.GET('/api-service-manage/enterprise',params)
  },

  addEnterprise:params=>{
    return API.POST('/api-service-manage/enterprise/add',params)
  },

  editEnterprise:params=>{
    return API.POST('/api-service-manage/enterprise/edit',params)
  },

  deleteEnterprise:params=>{
    return API.POST('/api-service-manage/enterprise/delete',params)
  },

  getById:params=>{
    return API.GET('/api-service-manage/enterprise/getById',params)
  },

  getList:params=>{
    return API.GET('/api-service-manage/enterprise/getList',params)
  },

  changeState:params=>{
    return API.POST('/api-service-manage/enterprise/changeState',params)
  }
}
