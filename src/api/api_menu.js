/**
 * 菜单相关的api
 * @author bootdo 1992lcg@163.com
 */
import * as API from './'

export default {

  menus: params=>{
    return API.GET('/api-base/menu',params)
  },
  editMenu: params=>{
    return API.PUT('/api-base/menu',params)
  },
  menuIdsByRoleId: params=>{
    return API.GET('/api-base/menu/roleId',params)
  },
  add: params=>{
    return API.POST('/api-base/menu',params)
  },
  remove: params=>{
    return API.DELETE('/api-base/menu',params)
  }
}
