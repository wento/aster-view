import * as API from './'

export default {

  /**
   * 获取医院列表（分页）
   */
  findList:params=>{
    return API.GET('/api-service-manage/hospital',params)
  },

  addHospital:params=>{
    return API.POST('/api-service-manage/hospital/add',params)
  },

  editHospital:params=>{
    return API.POST('/api-service-manage/hospital/edit',params)
  },

  deleteHospital:params=>{
    return API.POST('/api-service-manage/hospital/delete',params)
  },

  getById:params=>{
    return API.GET('/api-service-manage/hospital/getById',params)
  },

  changeState:params=>{
    return API.POST('/api-service-manage/hospital/changeState',params)
  }
}
